https://www.pexels.com/photo/person-holding-black-tablet-computer-6037394/

In this brief article, we talk about the problem of Macbeth's poem.
Macbeth is a story that involves the title character’s passion for power.
Three witches come to [Macbeth and prophecy](https://shakespeare.folger.edu/shakespeares-works/macbeth/act-4-scene-1/) to him that he would one day be king.

Macbeth informs his wife about the prophecy.
The prophecy brings conflict for Macbeth so he resolves to kill King Duncan to actualize the prophecy.
Macbeth is himself an admirable war star, his obsession with power and advancement makes his wife push his urge to fulfill the witches’ prophecy.
The decision to murder the king gives Macbeth sleepless nights. His ambition stirs his urge to commit murder.
He finally murders King Duncan and later wallows in guilty.

His morality takes over and the consequences overwhelm him.
He abandons his morals due to his overriding ambition and thirst for power even though he is a respected soldier.
Click this link https://writingbros.com/essay-examples/macbeth/ to read more essay examples on Macbeth.
Let’s get a brief plot analysis so we can understand the Macbeth story better.

**Plot Analysis of the Macbeth Poem**

At the beginning of the play, Macbeth conflicts with himself as he struggles to decide whether he will become the king.
Even though Lady Macbeth advises him what to do, he is still indecisive.
Macbeth then stops struggling against his ambition to seize power and the conflict shifts to Macbeth and other characters- Banquo and Macduff who contest his authority.

Macbeth is the hero and the main focus of the play so audiences can access his perception.
But Macbeth acts against his interests and the interests of other characters as well as his country. So he becomes the adversary.
His opposers finally defeat him to restore order and justice.
The urge to seize power sharpens when Lady Macbeth learns of the three witches’ prophecy that her husband will be the king.
So she starts to find ways to fulfill the prophecy.

This leaves Macbeth in a dilemma because he wants to be loyal to King Duncan but at the same time fulfill his wife’s urge- to murder Duncan.
During all this time, Lady Macbeth doesn’t appear to feel ashamed. She’s even not remorseful about the unthinkable act she is pushing her husband to commit.

All she focuses on is to ensure Macbeth can listen and heed her plot.
She’s committed to putting her intelligent and strategic ability to good use.
She finally manages to convince and manipulate her husband into taking action by telling him “when you durst do it, then you were a man”.
Remember that all this time, Macbeth is struggling whether to kill the king or stay loyal and ignore his wife’s plot.
Macbeth kills the king and seizes the power but Lady Macbeth flees in fear of being accused of the crime.

After seizing power, Macbeth faces another conflict with his opposition who mistrust his kingship and the way he got it.
Since he had to courage to murder King Duncan to take power, Macbeth shows no signs of giving up power.
Banquo is suspicious about how Macbeth achieved power. 

Macbeth’s reason to mistrust Banquo is that he knows he might be convinced by his heirs to challenge and murder him the same way [Lady Macbeth](https://en.wikipedia.org/wiki/Lady_Macbeth) did.
That’s just a snapshot of the conflict in Macbeth's poem.
The themes and symbols exhibited in the play are loyalty, fate, and free will, the symbolism of sleep and blood, ambition, appearance, and reality.
You can get this play and learn more about the problem it exhibits.
